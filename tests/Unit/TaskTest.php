<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Task;

class TaskTest extends TestCase
{
    /**
     * Create Task Test
     *
     * @return void
     */
    public function testCreateTask()
    {
        $data = [
            'name' => "task",
            'description' => "desc",
            'completed' => 0,
        ];

        $user = factory(User::class)->create();
        
        $response = $this->actingAs($user, 'api')
            ->post('api/task/create?api_token='.$user->api_token, $data)
            ->assertStatus(201);
    }
    /**
     * Delete Task Test
     *
     * @return void
     */
    public function testDeleteTask()
    {        
        $user = factory(User::class)->create();
        $task = factory(Task::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->actingAs($user)
            ->delete('/api/task/' . $task->id . '/delete?api_token='.$user->api_token)
            ->assertStatus(200);
    }
    /**
     * Update Task Test
     *
     * @return void
     */
    public function testUpdateTask()
    {        
        $user = factory(User::class)->create();
        $task = factory(Task::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->actingAs($user)
            ->delete('/api/task/' . $task->id . '/delete?api_token='.$user->api_token)
            ->assertStatus(200);
    }
}
