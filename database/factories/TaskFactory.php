<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Task;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Task::class, function (Faker $faker) {
    $users = User::pluck('id')->toArray();
    $order = Task::max('order') + 1;
    return [
        'name' => $this->faker->sentence,
        'description' => $this->faker->paragraph,
        'user_id' => $faker->randomElement($users),
        'order' => $order
    ];
});
