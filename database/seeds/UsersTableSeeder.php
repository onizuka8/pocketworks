<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "matteo",
            'email' => 'matteo@gmail.com',
            'password' => bcrypt('password'),
            'remember_token' => Str::random(10),
            'api_token' => Str::random(60),
        ]);
    }
}
