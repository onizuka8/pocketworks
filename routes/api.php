<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function() {
    Route::post('/task/create', 'TaskController@create')->name('createTask');
    Route::post('/task/reorder', 'TaskController@reorder')->name('reorderTasks');
    Route::post('/task/{id}/update', 'TaskController@update')->name('updateTask');
    Route::delete('/task/{id}/delete', 'TaskController@delete')->name('deleteTask');
    Route::get('/task/{id}', 'TaskController@view')->name('viewTask');
    Route::get('/task/{id}/complete', 'TaskController@complete')->name('completeTask');
    Route::get('/task/{id}/todo', 'TaskController@todo')->name('todoTask');
});