@extends('layouts.app')

@section('content')
<div class="pl-4 pr-4">
    <div class="row justify-content-center">
        <div class="col-12">            

            <h1 class='text-center'>Your Tasks</h1>

            <div id='app'>
                <taskslist :todos='{{$user->todos}}' :completed='{{$user->completed}}' :token='"{{$user->api_token}}"'></taskslist>
            </div>                
        </div>
    </div>
</div>
@endsection
