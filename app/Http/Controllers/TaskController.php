<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Task;

class TaskController extends Controller
{
    /**
     * View a Task
     *
     */
    public function view(Request $request, $id)
    {
        $user = User::where("api_token", $request->input("api_token"))->first();        
        if(!$user){
            return response('Invalid user.', 401);
        }

        $task = Task::find($id);
        if(!$task){
            return response('Task not found.', 400);
        }

        if($task->user_id != $user->id){
            return response('Task not available for this user', 403);
        }

        return response()->json($task);
    }

    /**
     * Create a Task
     *
     */
    public function create(Request $request)
    {
        $user = User::where("api_token", $request->input("api_token"))->first();        
        if(!$user){
            print "<br>";
            print $request->input("api_token");
            return response('Invalid user.', 401);
        }

        $validatedData = $request->validate([
            'name' => 'required|max:100'
        ]);

        $task = new Task();
        $order = Task::max('order') + 1;
        $task->name = $request->input('name');
        $task->description = $request->input('description');
        $task->order = $order;
        $task->user_id = $user->id;
        if($task->save()){
            return response()->json($task, 201);
        }
        return response(500);
    }

    /**
     * Update a Task
     *
     */
    public function update(Request $request, $id)
    {
        $user = User::where("api_token", $request->input("api_token"))->first();        
        if(!$user){
            return response('Invalid user.', 401);
        }

        $validatedData = $request->validate([
            'name' => 'required|max:100'
        ]);

        $task = Task::find($id);

        if(!$task){
            return response('Task not found.', 400);
        }
        
        if($task->user_id != $user->id){
            return response('Task not available for this user', 403);
        }

        $task->name = $request->input('name');
        $task->description = $request->input('description');
        if($task->save()){
            return response(200);
        }
        return response(500);
    }

    /**
     * Delete a Task
     *
     */
    public function delete(Request $request, $id)
    {
        $user = User::where("api_token", $request->input("api_token"))->first();        
        if(!$user){
            return response('Invalid user.', 401);
        }

        $task = Task::find($id);
        if(!$task){
            return response('Task not found.', 400);
        }

        if($task->user_id != $user->id){
            return response('Task not available for this user', 403);
        }

        if($task->delete()){
            return response(200);
        }
        return response(500);
    }

    /**
     * Reorder todos or completed tasks
     * 
     */
    public function reorder(Request $request)
    {
        $user = User::where("api_token", $request->input("api_token"))->first();
        foreach($request->sortOrder as $order){
            $task = Task::find($order['id']);
            if(!$task || $task->user_id != $user->id){
                //ignore task
                continue;
            }
            $task->order = $order['order'];
            $task->save();
        }
        return response(200);
    }

    /**
     * Set complete a Task
     *
     */
    public function complete(Request $request, $id)
    {
        $this->toggleCompletedTask($request, $id, 1);
    }

    /**
     * Set todo a Task
     *
     */
    public function todo(Request $request, $id)
    {
        $this->toggleCompletedTask($request, $id, 0);
    }

    /**
     * Toggle 'completed' field of a Task
     *
     */
    private function toggleCompletedTask($request, $id, $value)
    {
        $user = User::where("api_token", $request->input("api_token"))->first();        
        if(!$user){
            return response('Invalid user.', 401);
        }

        $task = Task::find($id);
        if(!$task){
            return response('Task not found.', 400);
        }

        if($task->user_id != $user->id){
            return response('Task not available for this user', 403);
        }

        $task->completed = $value;
        if($task->save()){
            return response(200);
        }
        return response(500);
    }
}
